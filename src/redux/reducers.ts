import { combineReducers } from "redux";
import * as types from "./actions-type";

export default combineReducers({
  djInfo(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_DJINFO:
        return payload;
      default:
    }
    return state;
  },
  playInfo(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_PLAYINFO:
        return payload;
      default:
    }
    return state;
  },
  canPlayList(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_CANPLAYLIST:
        return payload;
      default:
    }
    return state;
  },
  playPrivileges(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_PLAYPRIVILEGES:
        return payload;
      default:
    }
    return state;
  },
  recommendList(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_RECOMMENDLIST:
        return payload;
      default:
    }
    return state;
  },
  recommendDj(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_RECOMMENDDJ:
        return payload;
      default:
    }
    return state;
  },
  recommendNewSong(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_RECOMMENDNEWSONG:
        return payload;
      default:
    }
    return state;
  },
  recommend(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_RECOMMEND:
        return payload;
      default:
    }
    return state;
  },
  myCreateList(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_MYCREATELIST:
        return payload;
      default:
    }
    return state;
  },
  myCollectList(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_MYCOLLECTLIST:
        return payload;
      default:
    }
    return state;
  },
  currentSongId(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_CURRENTSONGID:
        return payload;
      default:
    }
    return state;
  },
  currentSongInfo(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_CURRENTSONGINFO:
        return payload;
      default:
    }
    return state;
  },
  currentSongIndex(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_CURRENTSONGINDEX:
        return payload;
      default:
    }
    return state;
  },
  playMode(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_PLAYMODE:
        return payload;
      default:
    }
    return state;
  },
  likeMusicList(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_LIKEMUSICLIST:
        return payload;
      default:
    }
    return state;
  },
  isPlaying(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_ISPLAYING:
        return payload;
      default:
    }
    return state;
  },
  recentTab(state = {}, action) {
    const { type, payload } = action;
    switch (type) {
      case types.SET_RECENTTAB:
        return payload;
      default:
    }
    return state;
  }
});
