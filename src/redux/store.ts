import { createStore, applyMiddleware, compose } from "redux";
import thunkMiddleware from "redux-thunk";
import rootReducer from "./reducers";

const composeEnhancers =
  typeof window === "object" &&
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      })
    : compose;

const middlewares = [thunkMiddleware];

if (process.env.NODE_ENV === "development") {
  middlewares.push(require("redux-logger").createLogger());
}

const enhancer = composeEnhancers(
  applyMiddleware(...middlewares)
  // other store enhancers if any
);
export default function configStore() {
  const store = createStore(
    rootReducer,
    {
      djInfo: {},
      playInfo: {
        coverImgUrl: "",
        name: "",
        playCount: 0,
        tags: [],
        creator: {
          avatarUrl: "",
          nickname: ""
        },
        tracks: []
      },
      canPlayList: [],
      playPrivileges: [],
      recommendList: [],
      recommendDj: [],
      recommendNewSong: [],
      recommend: [],
      myCreateList: [],
      myCollectList: [],
      currentSongId: "",
      currentSongInfo: { al: "" },
      currentSongIndex: 0,
      playMode: "loop",
      likeMusicList: [],
      isPlaying: false,
      recentTab: 0
    },
    enhancer
  );
  return store;
}
