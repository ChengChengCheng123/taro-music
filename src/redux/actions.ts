import * as types from "./actions-type";

export function setDjinfo(payload) {
  return {
    type: types.SET_DJINFO,
    payload
  };
}
export function setPlayinfo(payload) {
  return {
    type: types.SET_PLAYINFO,
    payload
  };
}
export function setCanplaylist(payload) {
  return {
    type: types.SET_CANPLAYLIST,
    payload
  };
}
export function setPlayprivileges(payload) {
  return {
    type: types.SET_PLAYPRIVILEGES,
    payload
  };
}
export function setRecommendlist(payload) {
  return {
    type: types.SET_RECOMMENDLIST,
    payload
  };
}
export function setRecommenddj(payload) {
  return {
    type: types.SET_RECOMMENDDJ,
    payload
  };
}
export function setRecommendnewsong(payload) {
  return {
    type: types.SET_RECOMMENDNEWSONG,
    payload
  };
}
export function setRecommend(payload) {
  return {
    type: types.SET_RECOMMEND,
    payload
  };
}
export function setMycreatelist(payload) {
  return {
    type: types.SET_MYCREATELIST,
    payload
  };
}
export function setMycollectlist(payload) {
  return {
    type: types.SET_MYCOLLECTLIST,
    payload
  };
}
export function setCurrentsongid(payload) {
  return {
    type: types.SET_CURRENTSONGID,
    payload
  };
}
export function setCurrentsonginfo(payload) {
  return {
    type: types.SET_CURRENTSONGINFO,
    payload
  };
}
export function setCurrentsongindex(payload) {
  return {
    type: types.SET_CURRENTSONGINDEX,
    payload
  };
}
export function setPlaymode(payload) {
  return {
    type: types.SET_PLAYMODE,
    payload
  };
}
export function setLikemusiclist(payload) {
  return {
    type: types.SET_LIKEMUSICLIST,
    payload
  };
}
export function setIsplaying(payload) {
  return {
    type: types.SET_ISPLAYING,
    payload
  };
}
export function setRecenttab(payload) {
  return {
    type: types.SET_RECENTTAB,
    payload
  };
}

export function updateCanplayList(canPlayList, id) {
  return (dispatch, getState) => {
    let index = canPlayList.findIndex(item => item.id == id);
    dispatch(setCanplaylist(canPlayList));
    dispatch(setCurrentsongindex(index));
  };
}
export function setSonginfo(info) {
  return (dispatch, getState) => {
    const { canPlayList } = getState();
    let index = canPlayList.findIndex(item => item.id == info.id);
    let temp = canPlayList.map((item, index) => {
      item.current = false;
      if (index === index) {
        item.current = true;
      }
      return item;
    });
    dispatch(setCanplaylist(temp));
    dispatch(setCurrentsonginfo(info));
    dispatch(setCurrentsongindex(index));
  };
}
