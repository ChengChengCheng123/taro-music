import { useState, useCallback, useEffect } from "react";
import Taro from "@tarojs/taro";
import { AtSearchBar, AtIcon } from "taro-ui";
import { View, Text, Image, ScrollView } from "@tarojs/components";
import Loading from "../../components/Loading/Loading";
import {
  getKeywordInHistory,
  setKeywordInHistory,
  clearKeywordInHistory
} from "../../utils/index";
import { connect } from "react-redux";
import { searchHot } from "../../api/base";
import classnames from "classnames";
import "./index.scss";

type HotItem = {
  searchWord: string;
  score: number;
  iconUrl: string;
  content: string;
  iconType: number;
};

function Search() {
  const [searchValue, setSearchValue] = useState<string>("");
  const [hotList, setHotList] = useState<Array<HotItem>>([]);
  const [historyList, setHistoryList] = useState(() => getKeywordInHistory());
  useEffect(() => {
    searchHot().then(res => {
      setHotList(res.data);
    });
  }, []);
  const searchResult = useCallback(() => {
    goResult(searchValue);
  }, [searchValue]);
  const goResult = useCallback(val => {
    setKeywordInHistory(val);
    Taro.navigateTo({
      url: `/pages/searchResult/index?keywords=${val}`
    });
  }, []);

  return (
    <View className="search">
      <AtSearchBar
        actionName="搜一下"
        value={searchValue}
        onChange={e => setSearchValue(e.trim())}
        onActionClick={() => {
          searchResult();
        }}
        onConfirm={() => {
          searchResult();
        }}
        focus
        className="search__input"
        fixed
      />
      <ScrollView className="search-content" scrollY>
        {historyList.length ? (
          <View className="search-history">
            <View className="search-title flex between mb10">
              <Text className=" xl">搜索历史</Text>
              <AtIcon
                prefixClass="fa"
                value="trash-o"
                size="20"
                color="#cccccc"
                className=""
                onClick={() => {
                  clearKeywordInHistory();
                  setHistoryList([]);
                }}
              ></AtIcon>
            </View>
            <ScrollView scrollX className="history-list flex wrap">
              {historyList.map(keyword => (
                <Text
                  className="history-item"
                  key={keyword}
                  onClick={() => goResult(keyword)}
                >
                  {keyword}
                </Text>
              ))}
            </ScrollView>
          </View>
        ) : null}
        <View className="search-hot mt10">
          <View className="item xl">热搜榜</View>
        </View>
        {hotList.length === 0 ? <Loading /> : ""}
        <View className="hot-list">
          {hotList.map((item, index) => (
            <View
              key={item.searchWord}
              onClick={() => goResult(item.searchWord)}
              className="flex middle item"
            >
              <View className={classnames("hot-index", { spec: index <= 2 })}>
                {index + 1}
              </View>
              <View className="cell-main">
                <View className="flex middle">
                  <Text
                    className={classnames("hot-name", { spec: index <= 2 })}
                  >
                    {item.searchWord}
                  </Text>
                  <Text className="hot-score">{item.score}</Text>
                  {item.iconUrl ? (
                    <Image
                      src={item.iconUrl}
                      mode="widthFix"
                      className={classnames("hot-ico", {
                        spec: item.iconType === 5
                      })}
                    />
                  ) : (
                    ""
                  )}
                </View>
                <View className="hot-desc"> {item.content}</View>
              </View>
            </View>
          ))}
        </View>
      </ScrollView>
    </View>
  );
}
Search.config = {
  navigationBarTitleText: "搜索"
};
export default connect(
  function mapStateToProps(state) {
    return state;
  },
  function mapDispatchToProps(dispatch) {
    return { dispatch };
  }
)(Search);
