import { useCallback, useEffect } from "react";
import Taro, { useRouter } from "@tarojs/taro";
import { connect } from "react-redux";
import Loading from "../../components/Loading/Loading";
import Music from "../../components/Music/Music";
import { View, Image, Text } from "@tarojs/components";
import classnames from "classnames";
import { getPlayListDetail } from "../../redux/async";
import "./index.scss";
import { formatCount } from "../../utils/index";
import { setIsplaying } from "../../redux/actions";

function PlayListDetail(props) {
  const { id, name } = useRouter().params;
  const {
    dispatch,
    playInfo,
    isPlaying,
    currentSongInfo,
    playPrivileges,
    canPlayList
  } = props;
  useEffect(() => {
    Taro.setNavigationBarTitle({
      title: name || "music"
    });
    dispatch(getPlayListDetail(id));
  }, [id, name]);
  const togglePlay = useCallback(bool => {
    dispatch(setIsplaying(bool));
  }, []);
  const playSong = useCallback((songId, playStatus) => {
    if (playStatus === 0) {
      Taro.navigateTo({
        url: `/pages/songDetail/index?id=${songId}`
      });
    } else {
      Taro.showToast({
        title: "暂无版权",
        icon: "none"
      });
    }
  }, []);

  return (
    <View className={classnames("play", { musicbox: !!currentSongInfo.name })}>
      {currentSongInfo.id ? (
        <Music
          songInfo={{
            currentSongInfo,
            isPlaying,
            canPlayList
          }}
          isHome
          onUpdatePlayStatus={togglePlay}
        />
      ) : null}
      <View className="play-head">
        <Image className="play-bg" src={playInfo.coverImgUrl}></Image>
        <View className="play-cover">
          <Image className="play-img" src={playInfo.coverImgUrl}></Image>
          <Text className="play-tag">歌单</Text>
          <View className="play-num">
            <Text className="at-icon at-icon-sound ml10"></Text>
            {formatCount(playInfo.playCount)}
          </View>
        </View>
        <View className="play-info">
          <View className="head-title mb30">{playInfo.name}</View>
          <View className="flex middle mt10">
            <Image
              className="play-avatar mr5"
              src={playInfo.creator.avatarUrl}
            />
            <Text className="l">{playInfo.creator.nickname}</Text>
          </View>
        </View>
      </View>
      <View className="play-detail">
        <View className="flex wrap mb20">
          <View className="tag mr10"> 标签：</View>
          {playInfo.tags.map(tag => (
            <Text key={tag} className="tag-item">
              {tag}
            </Text>
          ))}
          {playInfo.tags.length === 0 ? "暂无" : ""}
        </View>
        <View className="l">简介：{playInfo.description || "暂无"}</View>
      </View>
      <View className="play-list">
        <View className="play-title">歌曲列表</View>
        {playInfo.tracks.length === 0 ? <Loading /> : ""}
        <View className="song-list">
          {playInfo.tracks.map((e, i) => (
            <View
              onClick={() => {
                playSong(e.id, playPrivileges[i].st);
              }}
              key={e.id}
              className={classnames("flex middle play-item", {
                disabled: playPrivileges[i].st === -200
              })}
            >
              <Text className="play-index">{i + 1}</Text>
              <View className="cell-main">
                <View className="xl mb10"> {e.name}</View>
                <View className="col-hui l">
                  {e.ar[0] ? e.ar[0].name : ""} - {e.al.name}
                </View>
              </View>
              <Text className="at-icon col-hui xl at-icon-chevron-right"></Text>
            </View>
          ))}
        </View>
      </View>
    </View>
  );
}
PlayListDetail.config = {
  navigationBarTitleText: "我的"
};
export default connect(
  function mapStateToProps(state) {
    return state;
  },
  function mapDispatchToProps(dispatch) {
    return { dispatch };
  }
)(PlayListDetail);
