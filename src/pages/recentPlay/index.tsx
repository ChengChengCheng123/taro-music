import { useState, useCallback, useEffect } from "react";
import Taro from "@tarojs/taro";
import { AtTabs, AtTabsPane } from "taro-ui";
import { View } from "@tarojs/components";
import { connect } from "react-redux";
import classnames from "classnames";
import Loading from "../../components/Loading/Loading";
import Music from "../../components/Music/Music";
import { setIsplaying } from "../../redux/actions";
import { userRecord } from "../../api/base";
import { MusicItemType } from "../../constants/types";
import "./index.scss";

const tabList = [
  {
    title: "最近7天"
  },
  {
    title: "全部"
  }
];
type PlayList = Array<{
  playCount: number;
  song: MusicItemType;
}>;
function RecentPlay(props) {
  const {
    dispatch,

    isPlaying,
    currentSongInfo,
    canPlayList
  } = props;

  const userId = Taro.getStorageSync("userId");
  const [list, setList] = useState<PlayList>([]);
  const [alls, setAlls] = useState<PlayList>([]);
  const [currentTab, setCurrentTab] = useState(0);

  useEffect(() => {
    userRecord({ uid: userId, type: 0 }).then(res => {
      setAlls(res.allData);
    });
    userRecord({ uid: userId, type: 1 }).then(res => {
      setList(res.weekData);
    });
  }, [userId]);
  const togglePlay = useCallback(bool => {
    dispatch(setIsplaying(bool));
  }, []);
  const playSong = useCallback((songId, playStatus) => {
    if (playStatus === 0) {
      Taro.navigateTo({
        url: `/pages/songDetail/index?id=${songId}`
      });
    } else {
      Taro.showToast({
        title: "暂无版权",
        icon: "none"
      });
    }
  }, []);
  return (
    <View
      className={classnames("recent", { musicbox: !!currentSongInfo.name })}
    >
      {currentSongInfo.id ? (
        <Music
          songInfo={{
            currentSongInfo,
            isPlaying,
            canPlayList
          }}
          isHome
          onUpdatePlayStatus={togglePlay}
        />
      ) : null}
      <AtTabs
        current={currentTab}
        swipeable={false}
        tabList={tabList}
        onClick={e => {
          setCurrentTab(e);
        }}
      >
        <AtTabsPane current={currentTab} index={0}>
          {list.length === 0 ? (
            <Loading />
          ) : (
            list.map(item => (
              <View
                key={item.song.id}
                onClick={() => {
                  playSong(item.song.id, item.song.st !== -200);
                }}
                className="recent-item flex middle"
              >
                <View className="cell-main">
                  <View className="recent-name mb10"> {item.song.name}</View>
                  <View className="recent-desc">
                    {" "}
                    {`${item.song.ar[0] ? item.song.ar[0].name : ""} - ${
                      item.song.al.name
                    }`}
                  </View>
                </View>
                <View className="fa fa-ellipsis-v recent-ico"></View>
              </View>
            ))
          )}
        </AtTabsPane>
        <AtTabsPane current={currentTab} index={1}>
          {alls.length === 0 ? (
            <Loading />
          ) : (
            alls.map(item => (
              <View
                key={item.song.id}
                onClick={() => {
                  playSong(item.song.id, item.song.st !== -200);
                }}
                className="recent-item flex middle"
              >
                <View className="cell-main">
                  <View className="recent-name"> {item.song.name}</View>
                  <View className="recent-desc">
                    {" "}
                    {`${item.song.ar[0] ? item.song.ar[0].name : ""} - ${
                      item.song.al.name
                    }`}
                  </View>
                </View>
                <View className="fa fa-ellipsis-v recent-ico"></View>
              </View>
            ))
          )}
        </AtTabsPane>
      </AtTabs>
    </View>
  );
}
RecentPlay.config = {
  navigationBarTitleText: "我的"
};
export default connect(
  function mapStateToProps(state) {
    return state;
  },
  function mapDispatchToProps(dispatch) {
    return { dispatch };
  }
)(RecentPlay);
