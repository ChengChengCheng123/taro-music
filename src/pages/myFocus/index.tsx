import { useState, useCallback, useEffect } from "react";
import Taro from "@tarojs/taro";
import { View, ScrollView } from "@tarojs/components";
import { connect } from "react-redux";
import { getfollows } from "../../api/base";
import UserListItem from "../../components/UserListItem/UserListItem";
import Loading from "../../components/Loading/Loading";
import "./index.scss";

type userList = Array<{
  avatarUrl: string;
  nickname: string;
  signature?: string;
  gender: number;
  userId: number;
}>;
function MyFans(props) {
  const {} = props;
  const [userId] = useState(() => Taro.getStorageSync("userId"));
  const [userList, setUserList] = useState<userList>([]);
  const [hasMore, setHasMore] = useState<boolean>(true);
  const getFollowedList = useCallback(() => {
    getfollows({ uid: userId, limit: 20, offset: userList.length }).then(
      res => {
        setUserList(userList.concat(res.follow));
        setHasMore(res.more);
      }
    );
  }, [userId]);
  useEffect(() => {
    getFollowedList();
  }, [userId]);
  return (
    <View className="fans">
      <ScrollView
        scrollY
        onScrollToLower={() => {
          getFollowedList();
        }}
        className="fans-list"
      >
        {userList &&
          userList.map(item => (
            <UserListItem userInfo={item} key={item.userId} />
          ))}
        <Loading hide={!hasMore} />
      </ScrollView>
    </View>
  );
}
MyFans.config = {
  navigationBarTitleText: "我的粉丝"
};
export default connect(
  function mapStateToProps(state) {
    return state;
  },
  function mapDispatchToProps(dispatch) {
    return { dispatch };
  }
)(MyFans);
