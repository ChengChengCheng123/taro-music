import { useEffect, useCallback } from "react";
import Taro, { useRouter } from "@tarojs/taro";
import { connect } from "react-redux";
import Loading from "../../components/Loading/Loading";
import Music from "../../components/Music/Music";
import { View, Image, Text } from "@tarojs/components";
import classnames from "classnames";
import { getDjDetail } from "../../redux/async";
import "./index.scss";
import { formatCount } from "../../utils/index";
import { setIsplaying } from "../../redux/actions";

function PlayListDetail(props) {
  const { id, name } = useRouter().params;
  const {
    dispatch,
    isPlaying,
    djInfo,
    currentSongInfo,
    playPrivileges,
    canPlayList
  } = props;
  useEffect(() => {
    Taro.setNavigationBarTitle({
      title: name || "music"
    });
    dispatch(getDjDetail(id));
  }, [id, name]);
  const togglePlay = useCallback(bool => {
    dispatch(setIsplaying(bool));
  }, []);
  const playSong = useCallback((songId, playStatus) => {
    if (playStatus === 0) {
      Taro.navigateTo({
        url: `/pages/songDetail/index?id=${songId}`
      });
    } else {
      Taro.showToast({
        title: "暂无版权",
        icon: "none"
      });
    }
  }, []);

  return (
    <View className={classnames("play", { musicbox: !!currentSongInfo.name })}>
      {currentSongInfo.id ? (
        <Music
          songInfo={{
            currentSongInfo,
            isPlaying,
            canPlayList
          }}
          isHome
          onUpdatePlayStatus={togglePlay}
        />
      ) : null}
      <View className="play-head">
        <Image className="play-bg" src={djInfo.blurCoverUrl}></Image>
        <View className="play-cover">
          <Image className="play-img" src={djInfo.coverUrl}></Image>
          <Text className="play-tag">歌单</Text>
          <View className="play-num">
            <Text className="at-icon at-icon-sound ml10"></Text>
            {formatCount(djInfo.playCount)}
          </View>
        </View>
        <View className="play-info">
          <View className="head-title mb30">{djInfo.name}</View>
        </View>
      </View>
      <View className="play-detail">
        <View className="l">简介：{djInfo.description || "暂无"}</View>
      </View>
      <View className="play-list">
        <View className="play-title">歌曲列表</View>
        {djInfo.songs.length === 0 ? <Loading /> : ""}
        <View className="song-list">
          {djInfo.songs &&
            djInfo.songs.map((e, i) => (
              <View
                onClick={() => {
                  playSong(e.id, playPrivileges[i].st);
                }}
                key={e.id}
                className={classnames("flex middle play-item")}
              >
                <Text className="play-index">{i + 1}</Text>
                <View className="cell-main">
                  <View className="xl mb10"> {e.name}</View>
                  <View className="col-hui l">
                    {e.album.company} - {e.album.subType}
                  </View>
                </View>
                <Text className="at-icon col-hui xl at-icon-chevron-right"></Text>
              </View>
            ))}
        </View>
      </View>
    </View>
  );
}
PlayListDetail.config = {
  navigationBarTitleText: "我的"
};
export default connect(
  function mapStateToProps(state) {
    return state;
  },
  function mapDispatchToProps(dispatch) {
    return { dispatch };
  }
)(PlayListDetail);
