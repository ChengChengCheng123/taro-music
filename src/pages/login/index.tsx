import { useState, useCallback } from "react";
import Taro from "@tarojs/taro";
import { View, Input } from "@tarojs/components";

import "./index.scss";
import { connect } from "react-redux";
import { AtButton, AtIcon } from "taro-ui";
import MusicTitle from "../../components/MusicTitle/MusicTitle";
import { login } from "../../api/base";

function Login(props) {
  const [phone, setPhone] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const toLogin = useCallback(() => {
    login({ phone, password }).then(res => {
      Taro.setStorageSync("userInfo", res);
      Taro.setStorageSync("userId", res.account.id);
      Taro.navigateTo({
        url: "/pages/index/index"
      });
    });
  }, [phone, password]);
  return (
    <View className="login">
      <MusicTitle isFixed={false} />
      <View className="login-content">
        <View className="flex middle login-item">
          <AtIcon value="iphone" size="24" color="#ccc"></AtIcon>
          <Input
            type="text"
            placeholder="手机号"
            className="login-input cell-main"
            value={phone}
            onInput={e => {
              setPhone(e.detail.value.trim());
            }}
          />
        </View>
        <View className="flex middle login-item">
          <AtIcon value="lock" size="24" color="#ccc"></AtIcon>
          <Input
            type="password"
            placeholder="密码"
            className="login-input cell-main"
            value={password}
            onInput={e => {
              setPassword(e.detail.value.trim());
            }}
          />
        </View>
        <AtButton className="login-btn" onClick={() => toLogin()}>
          登录
        </AtButton>
      </View>
    </View>
  );
}
Login.config = {
  navigationBarTitleText: "云音乐"
};
export default connect(
  function mapStateToProps(state) {
    return state;
  },
  function mapDispatchToProps(dispatch) {
    return { dispatch };
  }
)(Login);
