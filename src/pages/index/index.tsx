import { useState, useCallback, useEffect } from "react";
import Taro from "@tarojs/taro";
import { View, Image, Text, Swiper, SwiperItem } from "@tarojs/components";
import "./index.scss";
import { connect } from "react-redux";
import { getrecomandinfo } from "../../redux/async";
import { setIsplaying } from "../../redux/actions";
import { getBanner } from "../../api/base";
import Loading from "../../components/Loading/Loading";
import Music from "../../components/Music/Music";
import classnames from "classnames";
import { AtTabBar, AtSearchBar, AtIcon } from "taro-ui";
import { formatCount } from "../../utils/index";

function Index(props) {
  const {
    recommendList,
    recommendDj,
    currentSongInfo,
    isPlaying,
    canPlayList,

    dispatch
  } = props;
  // const dispatch = useDispatch()
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [current] = useState<number>(0);
  const [bannerList, setBannerList] = useState<any[]>([]);
  const [searchValue] = useState<string>("");
  useEffect(() => {
    dispatch(getrecomandinfo());
  }, []);
  useEffect(() => {
    getBanner({ type: 2 }).then(res => {
      setBannerList(res.banners);
    });
  }, []);
  useEffect(() => {
    if (recommendList.length || recommendDj.length) {
      setIsLoading(false);
    }
  }, [recommendList, recommendDj]);
  const togglePlay = useCallback(bool => {
    dispatch(setIsplaying(bool));
  }, []);
  const goSearch = useCallback(() => {
    Taro.navigateTo({
      url: `/pages/search/index`
    });
  }, []);
  const goDetail = useCallback(e => {
    Taro.navigateTo({
      url: `/pages/playList/index?id=${e.id}&name=${e.name}`
    });
  }, []);
  const goDjDetail = useCallback(e => {
    Taro.navigateTo({
      url: `/pages/djList/index?id=${e.id}&name=${e.name}`
    });
  }, []);
  const switchTab = useCallback(e => {
    if (e !== 1) return;
    Taro.reLaunch({
      url: "/pages/my/index"
    });
  }, []);
  return (
    <View className={classnames("index", { musicbox: !!currentSongInfo.name })}>
      <Loading fullPage hide={!isLoading} />
      {currentSongInfo.id ? (
        <Music
          songInfo={{
            currentSongInfo,
            isPlaying,
            canPlayList
          }}
          isHome
          onUpdatePlayStatus={togglePlay}
        />
      ) : null}

      <View className="search" onClick={() => goSearch()}>
        <AtSearchBar
          actionName="搜一下"
          disabled
          value={searchValue}
          onChange={() => goSearch()}
        />
      </View>
      <Swiper
        className="banner"
        indicatorColor="#999"
        indicatorActiveColor="#d43c33"
        circular
        indicatorDots
        autoplay
      >
        {bannerList.map(item => (
          <SwiperItem key={item.targetId} className="banner-item">
            <Image src={item.pic} className="banner-img" />
          </SwiperItem>
        ))}
      </Swiper>
      <View className="card-list flex middle">
        <View className="card-item ">
          <View className="card-col mb5">
            <AtIcon
              prefixClass="fa"
              value="calendar-minus-o"
              size="20"
              color="#ffffff"
            ></AtIcon>
          </View>

          <Text className="m">每日推荐</Text>
        </View>
        <View className="card-item">
          <View className="card-col mb5">
            <AtIcon
              prefixClass="fa"
              value="bar-chart"
              size="20"
              color="#ffffff"
            ></AtIcon>
          </View>
          <Text className="m">排行榜</Text>
        </View>
      </View>
      <View className="play-list">
        <View className="title l item">推荐歌单</View>
        <View className="play-contain flex  wrap">
          {recommendList.map(e => (
            <View
              onClick={() => {
                goDetail(e);
              }}
              key={e.id}
              className="play-item"
            >
              <View className="play-imgbox">
                <Image
                  src={`${e.picUrl}?imageView&thumbnail=250x0`}
                  className="play-pic"
                />
                <View className="play-count"> {formatCount(e.playCount)}</View>
              </View>
              <View className="row2 m play-name">{e.name}</View>
            </View>
          ))}
        </View>
      </View>
      <View className="play-list">
        <View className="title l item">推荐电台</View>
        <View className="play-contain flex  wrap">
          {recommendDj.map(e => (
            <View
              onClick={() => {
                goDjDetail(e);
              }}
              key={e.id}
              className="play-item"
            >
              <View className="play-imgbox">
                <Image
                  src={`${e.picUrl}?imageView&thumbnail=250x0`}
                  className="play-pic"
                />
                <View className="play-count"> {formatCount(e.playCount)}</View>
              </View>
              <View className="row2 m play-name">{e.name}</View>
            </View>
          ))}
        </View>
      </View>
      <AtTabBar
        fixed
        selectedColor="#d43c33"
        tabList={[
          { title: "发现", iconPrefixClass: "fa", iconType: "feed" },
          { title: "我的", iconPrefixClass: "fa", iconType: "music" }
        ]}
        onClick={e => {
          switchTab(e);
        }}
        current={current}
      />
    </View>
  );
}

Index.config = {
  navigationBarTitleText: "云音乐"
};
export default connect(
  function mapStateToProps(state) {
    return state;
  },
  function mapDispatchToProps(dispatch) {
    return { dispatch };
  }
)(Index);
