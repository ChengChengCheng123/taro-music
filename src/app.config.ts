export default defineAppConfig({
  pages: [
    "pages/index/index",
    "pages/my/index",
    "pages/videoDetail/index",
    "pages/search/index",
    "pages/djList/index",
    "pages/searchResult/index",
    // 'pages/search/index',
    "pages/songDetail/index",
    "pages/myFans/index",
    "pages/myFocus/index",
    "pages/login/index",
    "pages/playList/index",
    "pages/recentPlay/index"
  ],
  requiredBackgroundModes: ["audio"],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "WeChat",
    navigationBarTextStyle: "black"
  }
});
