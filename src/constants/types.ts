export type MusicItemType = {
  name: string
  id: number
  ar: Array<{ name: string }>
  al: { name: string }
  song: { id: number }
  copyright: number
  st?: number
  current?: boolean
}

export type currentSongInfoType = {
  id: number
  name: string
  ar: Array<{ name: string }>
  al: {
    picUrl: string
    name: string
  }
  url: string
  lrcInfo: any
  dt: number
  st: number
}
