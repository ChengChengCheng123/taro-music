import { AtIcon } from "taro-ui";
import { FC, memo } from "react";
import { View, Image } from "@tarojs/components";
import "./style.scss";

type Props = {
  userInfo: {
    avatarUrl: string;
    nickname: string;
    signature?: string;
    gender: number;
    userId: number;
  };
};

const UserListItem: FC<Props> = ({ userInfo }) => {
  if (!userInfo) return null;
  return (
    <View className="user flex middle">
      <Image
        src={`${userInfo.avatarUrl}?imageView&thumbnail=250x0`}
        className="user-avatar"
      />
      <View className="cell-main">
        <View className="user-name">
          {userInfo.nickname}
          {userInfo.gender === 1 ? (
            <AtIcon
              prefixClass="fa"
              value="mars"
              size="12"
              color="#5cb8e7"
              className="use-ico"
            ></AtIcon>
          ) : (
            ""
          )}
          {userInfo.gender === 2 ? (
            <AtIcon
              prefixClass="fa"
              value="venus"
              size="12"
              color="#f88fb8"
              className="use-ico venus"
            ></AtIcon>
          ) : (
            ""
          )}
        </View>
        <View className="user-sign">{userInfo.signature || ""}</View>
      </View>
    </View>
  );
};

export default memo(UserListItem, (prevProps, nextProps) => {
  if (nextProps.userInfo !== prevProps.userInfo) {
    return false; // 返回false本次则会渲染，反之则不会渲染
  }
  return true;
});
