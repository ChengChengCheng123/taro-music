import { FC, memo, useCallback } from "react";
import { View, Image } from "@tarojs/components";
import { AtIcon } from "taro-ui";
import classnames from "classnames";
import "./index.scss";
import { currentSongInfoType, MusicItemType } from "../../constants/types";

type Props = {
  songInfo: {
    currentSongInfo: currentSongInfoType;
    isPlaying: boolean;
    canPlayList: Array<MusicItemType>;
  };
  isHome?: Boolean;
  onUpdatePlayStatus: (object) => any;
};
const Music: FC<Props> = props => {
  const { songInfo, isHome } = props;
  let { currentSongInfo, isPlaying } = songInfo;
  const switchPlayStatus = useCallback(() => {
    console.log("switchPlayStatus");
  }, []);

  const setIsOpened = useCallback((bool: boolean) => {
    console.log("switchPlayStatus");
  }, []);
  return (
    <View className={classnames("music flex middle", { isHome: isHome })}>
      <Image
        src={currentSongInfo.al.picUrl}
        className={classnames("music-pic", { circling: isPlaying })}
      ></Image>
      <View className="music-info cell-main">
        <View className="name">{currentSongInfo.name}</View>
        <View className="l desc">
          {currentSongInfo.ar[0] ? currentSongInfo.ar[0].name : ""} -{" "}
          {currentSongInfo.al.name}
        </View>
      </View>
      <View className="music-icon">
        <AtIcon
          value={isPlaying ? "pause" : "play"}
          size="30"
          color="#FFF"
          onClick={() => switchPlayStatus()}
        ></AtIcon>
        <AtIcon
          value="playlist"
          size="30"
          color="#FFF"
          className="icon_playlist"
          onClick={() => setIsOpened(true)}
        ></AtIcon>
      </View>
    </View>
  );
};
Music.defaultProps = {
  songInfo: {
    currentSongInfo: {
      id: 0,
      name: "",
      ar: [],
      al: {
        picUrl: "",
        name: ""
      },
      url: "",
      lrcInfo: "",
      dt: 0, // 总时长，ms
      st: 0 // 是否喜欢
    },
    canPlayList: [],
    isPlaying: false
  }
};
export default memo(Music, (prevProps, nextProps) => {
  if (
    nextProps.songInfo.isPlaying !== prevProps.songInfo.isPlaying ||
    nextProps.songInfo.currentSongInfo.name !==
      prevProps.songInfo.currentSongInfo.name
  ) {
    return false; // 返回false本次则会渲染，反之则不会渲染
  }
  return true;
});
