import request from '../utils/require'

export function getSongList(data) {
  return request.get({
    url: `/playlist/detail`,
    data
  })
}
export function getRecommand() {
  return request.get({
    url: `/personalized`
  })
}
export function getDjprogram() {
  return request.get({
    url: `/personalized/djprogram`
  })
}
export function getNewSong() {
  return request.get({
    url: `/personalized/newsong`
  })
}
export function getRecomandPark() {
  return request.get({
    url: `/personalized/recommend`
  })
}
export function getSongDetail(data) {
  return request.get({
    url: `/song/detail`,
    data
  })
}
export function getSongUrl(data) {
  return request.get({
    url: `/song/url`,
    data
  })
}
export function getLyric(data) {
  return request.get({
    url: `/lyric`,
    data
  })
}
export function likeTo(data) {
  return request.get({
    url: `/like`,
    data
  })
}
export function likeList(data) {
  return request.get({
    url: `/likelist`,
    data
  })
}

//  banner
export function getBanner(data) {
  return request.get({
    url: `/banner`,
    data
  })
}

// login
export function login(data) {
  return request.get({
    url: `/login/cellphone`,
    data
  })
}

// user/detail
export function getuseInfo(data) {
  return request.get({
    url: `/user/detail`,
    data
  })
}
// user/playlist
export function getuseplay(data) {
  return request.get({
    url: `/user/playlist`,
    data
  })
}
// loginout
export function logout(data) {
  return request.get({
    url: `/logout`,
    data
  })
}
//
export function followeds(data) {
  return request.get({
    url: `/user/followeds`,
    data
  })
}
export function getfollows(data) {
  return request.get({
    url: `/user/follows`,
    data
  })
}
export function playDetail(data) {
  return request.get({
    url: `/playlist/detail`,
    data
  })
}
//
export function userRecord(data) {
  return request.get({
    url: `/user/record`,
    data
  })
}
export function searchHot(data) {
  return request.get({
    url: `/search/hot/detail`,
    data
  })
}
export function search(data) {
  return request.get({
    url: `/search`,
    data
  })
}
export function queryDj(data) {
  return request.get({
    url: `/dj/program/detail`,
    data
  })
}
export function likelist(data) {
  return request.get({
    url: `/likelist`,
    data
  })
}
export function setlike(data) {
  return request.get({
    url: `/like`,
    data
  })
}
export function mvDetail(data) {
  return request.get({
    url: `/mv/detail`,
    data
  })
}
export function artists(data) {
  return request.get({
    url: `/artists`,
    data
  })
}
export function mvUrl(data) {
  return request.get({
    url: `/mv/url`,
    data
  })
}
export function simiMv(data) {
  return request.get({
    url: `/simi/mv`,
    data
  })
}
export function comment(type, data) {
  return request.get({
    url: `/comment/${type}`,
    data
  })
}

export function videoDetail(data) {
  return request.get({
    url: `/video/detail`,
    data
  })
}
export function getvideoUrl(data) {
  return request.get({
    url: `/video/url`,
    data
  })
}
export function videoRelated(data) {
  return request.get({
    url: `/related/allvideo`,
    data
  })
}
export function checkMusic(data) {
  return request.get({
    url: `/check/music`,
    data
  })
}
