import Taro from "@tarojs/taro";

export const formatNumber = (n: number | string): string => {
  n = n.toString();
  return n[1] ? n : "0" + n;
};
/**
 *
 * @param {string} date 时间
 */
export const formatTime = (date: Date): string => {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const second = date.getSeconds();

  return (
    [year, month, day].map(formatNumber).join("-") +
    " " +
    [hour, minute, second].map(formatNumber).join(":")
  );
};
/**
 *
 * @param {string} name 错误名字
 * @param {string} action 错误动作描述
 * @param {string} info 错误信息，通常是 fail 返回的
 */

export const logError = (
  name: string,
  action: string,
  info?: string | object
) => {
  if (!info) {
    info = "empty";
  }
  let time = formatTime(new Date());
  console.error(time, name, action, info);
  if (typeof info === "object") {
    info = JSON.stringify(info);
  }
  Taro.showToast({
    title: info,
    icon: "error",
    duration: 2000
  });
};

// 转换歌词字符串为数组
export const parse_lrc = (lrc_content: string) => {
  let now_lrc: Array<{
    lrc_text: string;
    lrc_sec?: number;
  }> = []; // 声明一个临时数组
  let lrc_row: Array<string> = lrc_content.split("\n"); // 将原始的歌词通过换行符转为数组
  let scroll = true; // 默认scroll初始值为true
  for (let i in lrc_row) {
    if (lrc_row[i].indexOf("]") === -1 && lrc_row[i]) {
      now_lrc.push({ lrc_text: lrc_row[i] });
    } else if (lrc_row[i] !== "") {
      let tmp: string[] = lrc_row[i].split("]");
      for (let j in tmp) {
        scroll = false;
        let tmp2: string = tmp[j].substr(1, 8);
        let tmp3: any = tmp2.split(":");
        let lrc_sec: any = Number(tmp3[0] * 60 + Number(tmp3[1]));
        if (lrc_sec && lrc_sec > 0) {
          let lrc = tmp[tmp.length - 1].replace(/(^\s*)|(\s*$)/g, "");
          lrc && now_lrc.push({ lrc_sec: lrc_sec, lrc_text: lrc });
        }
      }
    }
  }
  if (!scroll) {
    now_lrc.sort(function(
      a: { lrc_sec: number; lrc_text: string },
      b: { lrc_sec: number; lrc_text: string }
    ): number {
      return a.lrc_sec - b.lrc_sec;
    });
  }
  return {
    now_lrc: now_lrc,
    scroll: scroll
  };
};
export const formatCount = num => {
  if (!num) {
    return 0;
  }
  num = num.toString();
  if (num.length <= 4) {
    return num;
  }
  let count = num.substr(0, num.length - 4);
  let thousands = Math.round(num.substr(-4, -2));
  return `${count}.${thousands}万`;
};
export function getUrl() {
  var pages = Taro.getCurrentPages(); //获取加载的页面
  var currentPage = pages[pages.length - 1]; //获取当前页面的对象
  var url = currentPage.route; //当前页面url
  var options = currentPage.options; //如果要获取url中所带的参数可以查看options
  var urlWithArgs = url + "?";
  for (var key in options) {
    var value = options[key];
    urlWithArgs += key + "=" + value + "&";
  }
  urlWithArgs = urlWithArgs.substring(0, urlWithArgs.length - 1);
  return urlWithArgs;
}
// 存储搜索关键字
export const setKeywordInHistory = (keyword: string) => {
  const keywordsList: Array<string> = Taro.getStorageSync("keywordsList") || [];
  const index = keywordsList.findIndex(item => item === keyword);
  if (index !== -1) {
    keywordsList.splice(index, 1);
  }
  keywordsList.unshift(keyword);
  Taro.setStorage({ key: "keywordsList", data: keywordsList });
};

// 获取搜索关键字
export const getKeywordInHistory = (): Array<string> => {
  return Taro.getStorageSync("keywordsList");
};

// 清除搜索关键字
export const clearKeywordInHistory = () => {
  Taro.removeStorageSync("keywordsList");
};
// 格式化时间戳为日期
export const formatTimeStampToTime = timestamp => {
  const date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
  const year = date.getFullYear();
  const month =
    date.getMonth() + 1 < 10
      ? "0" + (date.getMonth() + 1)
      : date.getMonth() + 1;
  const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
  // const hour = date.getHours() + ':';
  // const minutes = date.getMinutes() + ':';
  // const second = date.getSeconds();
  return `${year}-${month}-${day}`;
};
