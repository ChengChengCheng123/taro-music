import { FC } from "react";
import { Provider } from "react-redux";

import configStore from "./redux/store";

import "./app.scss";
import "./assets/css/index.scss";
import "taro-ui/dist/style/index.scss";
// 全局引入一次即可
const store = configStore();

const App: FC = ({ children }) => {
  return <Provider store={store}>{children}</Provider>;
};

export default App;
